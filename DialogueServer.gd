extends Node

enum emotion_state{relaxed=0,happy=1,angry=-1,anxious=2}
enum dialgoue_type{start=0,normal=1,event=2,end=-1}
var dialog_name:=""
var can_input:=false
@export var fade_speed:=0.1
var index:="10"
var dialogue_container:DialogueContainer
var instant:=false
var finished:=true
var in_dialog:=false
var starting:=false
var next:=true
var end:=false
var dialog
signal dailog_choice(type,emotion,voice)
func _ready():
	process_mode=Node.PROCESS_MODE_ALWAYS
func create_dialogue(audio=0,mode:=0,target=null)->DialogueContainer:
	if get_tree().root.get_node("HUD")!=null:
		dialogue_container=DialogueContainer.new(target,instant,audio,mode)
		dialogue_container.connect("choice",pick_a_choice)
		get_tree().root.get_node("HUD").add_child(dialogue_container)
		return dialogue_container
	else:
		print("Can't create missing Autoload named: HUD")
		return null

func start(new_dialog:=""):
	if new_dialog.is_empty() or  dialogue_container==null:
		return
	await dialogue_container.fade_in()
	dialog=get_dialgoue(new_dialog)
	var list:=[]
	finished=false
	for i in dialog.size():
		if dialog.values()[i]["type"]==dialgoue_type.start:
			list.append(dialog.keys()[i])
	index=list[randi_range(0,list.size()-1)]
	dialogue_change_gamestate(true)
	display_dialogue(dialog[index]["title"],dialog[index]["language_text"][TranslationServer.get_locale()],dialog[index]["choices"],dialog[index]["emot"],dialog[index]["type"])
	if dialogue_container.current_audio!=dialogue_container.audio.letters:
		dialogue_container.play_track.emit()
func _input(event):
	if event is InputEvent  and dialogue_container!=null:
		if event.is_action_pressed("ui_accept") and can_input:
			if starting:
				if !finished:
					if dialogue_container.can_skip():
						dialogue_container.skip()
						return
					if dialog[index]["choices"].is_empty() or !next:
						return
					next=false
					var new_index=dialog[index]["choices"][0]["index"]
					index=new_index
					display_dialogue(dialog[index]["title"],dialog[index]["language_text"][TranslationServer.get_locale()],dialog[index]["choices"],dialog[index]["emot"],dialog[index]["type"])
				else:
					dialogue_container.set_text("")
					await dialogue_container.fade_out()
					dialogue_change_gamestate(false)
					dialogue_container.queue_free()
					remove_connections()

func display_dialogue(title:String,text:String,choices:Array,current_emotion_state:emotion_state=0,dialogue_state:dialgoue_type=1)->void:
	if finished:
		return
	dialogue_container.set_text("")
	dialogue_container.set_title("[color="+get_emotion_color(current_emotion_state)+"]"+title+"[/color]")
	await dialogue_container.set_text(text_process(text,current_emotion_state))
	if choices.is_empty():
		dialogue_container.down_arrow_animation(false)
		finished=true
	elif choices.size()==1:
		dialogue_container.down_arrow_animation(true)
		next=true
	else:
		await dialogue_container.create_choices(choices)
	dailog_choice.emit(dialogue_state,current_emotion_state,"")

func pick_a_choice(val):#todo
	await dialogue_container.remove_choices()
	index=str(val)
	if dialog[index]["type"]==dialgoue_type.normal:
		display_dialogue(dialog[index]["title"],dialog[index]["language_text"][TranslationServer.get_locale()],dialog[index]["choices"],dialog[index]["emot"],dialog[index]["type"])
	elif dialog[index]["type"]==dialgoue_type.end:
		display_dialogue(dialog[index]["title"],dialog[index]["language_text"][TranslationServer.get_locale()],dialog[index]["choices"],dialog[index]["emot"],dialog[index]["type"])
	elif dialog[index]["type"]==dialgoue_type.event:
		dialogue_event(dialog[index]["event"])
		display_dialogue(dialog[index]["title"],dialog[index]["language_text"][TranslationServer.get_locale()],dialog[index]["choices"],dialog[index]["emot"],dialog[index]["type"])


func get_emotion_color(emot:=emotion_state.relaxed)->String:
	if emot ==emotion_state.happy:
		return "green"
	elif emot ==emotion_state.angry:
		return "red"
	elif emot ==emotion_state.anxious:
		return "yellow"
	else:
		return "white"


func text_process(text:String,emot)->String:
	var  new:= text.replace("[character]","[color="+get_emotion_color(emot)+"]"+dialog_name+"[/color]")
	return new

func dialogue_event(event):
	if get_tree().root.get_node_or_null("ReduxManager")!=null:
			if get_tree().root.get_node_or_null("ReduxManager").get_store("EventsManager")!=null:
				get_tree().root.get_node_or_null("ReduxManager").get_store("EventsManager").trigger(event)
func dialogue_change_gamestate(active:bool):
	can_input=active
	starting=active
	if get_tree().root.get_node_or_null("ReduxManager")!=null:
		if get_tree().root.get_node("ReduxManager").get_store("GameManager")!=null:
			if get_tree().root.get_node("UiManager")!=null:
				get_tree().root.get_node("UiManager").get_window("Pause Menu").active=!active
			if active:
				get_tree().root.get_node("ReduxManager").get_store("GameManager").CurrentGameState=4
			else:
				get_tree().root.get_node("ReduxManager").get_store("GameManager").CurrentGameState=3
func get_dialgoue(dialoge_path:=""):
	if get_tree().root.get_node("Loading")!=null:
		return get_tree().root.get_node("Loading")._load_json(dialoge_path)
	else:
		var file = File.new()
		if not file.file_exists(dialoge_path):
			return null
		file.open(dialoge_path, File.READ)
		var json=JSON.new()
		json.parse(file.get_as_text())
		var data = json.get_data()
		file.close()
		return data
func set_speed(index:=0):
	match index:
		0:instant=false
		1:instant=true

func remove_connections():
	for i in dailog_choice.get_connections():
		dailog_choice.disconnect(i["callable"])
